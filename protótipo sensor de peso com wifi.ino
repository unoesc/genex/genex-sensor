#include <HX711.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>


SoftwareSerial Serial1(7, 6);


char ssid[] = "NOME_DA_SUA_REDE_WIFI";
char pass[] = "SENHA_DA_SUA_REDE_WIFI";
char token[] = "TOKEN_DA_SUA_CONTA";


int status = WL_IDLE_STATUS;


String BASE_URL = "http://duducdi.com/";


#define pinDT  33
#define pinSCK  31


#define pesoMin 0.010
#define pesoMax 30.0


//Setar a escala de ajuste do sensor da balan�a como nao temos o sensor fica como 0
#define escala 0.0f
float medida=0;


void initSerial();
void initWiFi();
void httpRequest(String path);


WiFiClient client;
HTTPClient http;


HX711 scale;


void setup() {
  Serial.begin(57600);
  initSerial();
  initWiFi();

  scale.begin(pinDT, pinSCK); 
  scale.set_scale(escala); 

  delay(2000);
  scale.tare(); // ZERANDO A BALAN�A
}

void loop(){
  scale.power_up();
  medida = scale.get_units(5);
  Serial.print("Peso: ");
  Serial.println(medida);
  if (WiFi.status() != WL_CONNECTED){
    initWiFi();
  }
  if(medida > pesoMin && medida < pesoMax){
    httpRequest("/sensor-status");
  }
  scale.power_down();
  delay(1000*60*30); // 30 minutos
}



void httpRequest(String path)
{
  String payload = makeRequest(path);

  if (!payload) {
    return;
  }

  Serial.println("##[RESULT]## ==> " + payload);
}

String makeRequest(String path)
{
  http.begin(BASE_URL + path);
  http.addHeader("content-type", "application/x-www-form-urlencoded");

  String body = "sensor_token=" + token + "&lt=" + (static_cast<int>(medida)).toString();

  int httpCode = http.POST(body);

  if (httpCode < 0) {
    Serial.println("request error - " + httpCode);
    return "";
  }

  if (httpCode != HTTP_CODE_OK) {
    return "";
  }

  String response =  http.getString();
  http.end();

  return response;
}



void initSerial() {
  Serial.begin(115200);
}



void initWiFi() {
  delay(10);
  Serial.println("Conectando-se em: " + String(SSID));


  WiFi.begin(ssid, pass);
  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
  }
  Serial.println();
  Serial.print("Conectado na Rede " + String(SSID) + " | IP => ");
  Serial.println(WiFi.localIP());
}



