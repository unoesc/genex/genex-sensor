import network
import time
import urequests
from hx711 import HX711  # biblioteca externa

wifi = network.WLAN(network.STA_IF)

user_token = 'TOKEN_DO_USUARIO'

medida = None
balanca = HX711(29, 30, 1)

wifi_timeout = 0

tara = 0

calibragem = 0  # (peso_medido - tara)/peso_real = valor de calibragem

base_url = 'http://duducdi.com/'


def meansure():
    balanca.power_up()
    meansure = (balanca._read() - tara) / calibragem
    balanca.power_down()
    return meansure


while True:
    wifi.active(False)
    time.sleep(0.5)
    wifi.active(True)
    wifi.connect('SSID_DA_REDE_WIFI', 'SENHA_DA_REDE_WIFI')

    if not wifi.isconnected():
        while not wifi.isconnected() and wifi_timeout < 10:
            print('Conectando', (10 - wifi_timeout))
            wifi_timeout = wifi_timeout + 1
            time.sleep(1)

    if wifi.isconnected():
        print('Conectado')
        medida = meansure()
        if medida is not None:
            req = urequests.post(base_url + 'sensor-status', json={'token': user_token, 'lt': int(medida)})
            medida = None
            print(req.text)
            req.close()
        else:
            print('Erro ao ler a medida')
        time.sleep(60 * 30)
    else:
        print('Time out')
        time.sleep(60 * 30)



